<?php
  session_start();

  $user = $_SESSION['user'];
  if (!$user) {
    header('Location: index.php');
  }
  ?>

<h1> Bienvenido <?php echo $user['nombre'];?> <?php echo $user['apellido']; ?> </h1>
<a href="logout.php">Logout</a>

<nav class="nav">
    <?php  if($user['role'] === 'A') { ?>
      <?php header('Location: matri.php'); ?>
    
    <?php } ?> 
    <?php  if($user['role'] === 'E') { ?>
      <?php header('Location: es.php'); ?>
    
    <?php } ?> 
    <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Arboles</a>
    </li>
</nav>