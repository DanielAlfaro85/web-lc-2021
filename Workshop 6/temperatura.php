<?php
    echo "<pre>";

    $temperatures = array(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73);

    echo "Recorded temperatures : ";
    echo implode(", ", $temperatures);
    echo "<br>";

    echo "Average Temperature is : ";
    echo number_format(array_sum($temperatures) / count($temperatures), 1);
    echo "<br>";

    //sort the temperatures in ascending order for both of the following lists.
    sort($temperatures);

    //print the first 5 values
    echo "List of five lowest temperatures : ";
    echo implode(", ", array_slice(array_unique($temperatures), 0, 5));
    echo "<br>"; 

    //print the last 5 values
    echo "List of five highest temperatures : ";
    echo implode(", ", array_slice(array_unique($temperatures), -5, 5));
    echo "<br>";

    echo "</pre>";
?>